package com.darren.Strategy;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StrageyTest {
	
	@Test
	public void IsValidCard() {
		CreditCard amexCard = new CreditCard(new AmexStragety());
		
		amexCard.setNumber("379069706424510");
		amexCard.setDate("04/2020");
		amexCard.setCvv("123");
		
		assertThat(amexCard.isValid()).isTrue();

	}

	@Test
	public void IsNoValidCard() {
		CreditCard amexCard = new CreditCard(new AmexStragety());
		
		amexCard.setNumber("359185883464283");
		amexCard.setDate("04/2020");
		amexCard.setCvv("123");
		
		assertThat(amexCard.isValid()).isFalse();

	}
	
	@Test
	public void IsValidVisaCard() {
		CreditCard visaCard = new CreditCard(new VisaStragtegy());
		
		visaCard.setNumber("4556384735712174");
		visaCard.setDate("04/2020");
		visaCard.setCvv("123");
		
		assertThat(visaCard.isValid()).isTrue();

	}
	
	@Test
	public void IsInValidVisaCard() {
		CreditCard visaCard = new CreditCard(new VisaStragtegy());
		
		visaCard.setNumber("479185883464283");
		visaCard.setDate("04/2020");
		visaCard.setCvv("123");
		
		assertThat(visaCard.isValid()).isFalse();

	}


}
