package com.darren.Strategy;

import java.util.List;
import java.util.function.Consumer;

public class ShoppingCart {

	private List<Item> items;
		
	public ShoppingCart(List<Item> items) {
		super();
		this.items = items;
	}

	public void pay ( PaymentMethod method) {
		int total = cartTotal();
		if (method == PaymentMethod.CREDIT)
			System.out.println("Pay with credit " + total);
		else if (method == PaymentMethod.MONEY)
			System.out.println("Pay with cash " + total);
	}
	private int cartTotal() {
		// TODO Auto-generated method stub
		return 0;
	}
}
