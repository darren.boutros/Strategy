package com.darren.Strategy;

public enum PaymentMethod {

	CREDIT,
	MONEY;
}
